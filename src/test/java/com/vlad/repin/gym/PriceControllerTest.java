package com.vlad.repin.gym;


import com.vlad.repin.gym.dto.ResponseDto;
import com.vlad.repin.gym.dto.StatusType;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;

@Slf4j
public class PriceControllerTest extends GymCommonTest {

    private static final ParameterizedTypeReference<ResponseDto<Integer>>
            PRICE_PARAMETERIZED_TYPE_REFERENCE =
            new ParameterizedTypeReference<>() {
            };

    @Test
    void getSubscriptionPriceForOneMonthSuccessTest() {
        ResponseDto<Integer> response = makeGet(
                "api/v1/price/price/get?month=1",
                null,
                PRICE_PARAMETERIZED_TYPE_REFERENCE

        );
        log.info("response = {}", response);
        ResponseDto<Integer> expectedResponse = new ResponseDto<>(
                2000,
                StatusType.OK,
                null
        );

        Assertions
                .assertThat(response)
                .isEqualTo(expectedResponse);
    }
    @Test
    void getSubscriptionPriceForThreeMonthSuccessTest() {
        ResponseDto<Integer> response = makeGet(
                "api/v1/price/price/get?month=3",
                null,
                PRICE_PARAMETERIZED_TYPE_REFERENCE

        );
        log.info("response = {}", response);
        ResponseDto<Integer> expectedResponse = new ResponseDto<>(
                5700,
                StatusType.OK,
                null
        );

        Assertions
                .assertThat(response)
                .isEqualTo(expectedResponse);
    }
    @Test
    void getSubscriptionPriceForSixMonthSuccessTest() {
        ResponseDto<Integer> response = makeGet(
                "api/v1/price/price/get?month=6",
                null,
                PRICE_PARAMETERIZED_TYPE_REFERENCE

        );
        log.info("response = {}", response);
        ResponseDto<Integer> expectedResponse = new ResponseDto<>(
                10800,
                StatusType.OK,
                null
        );

        Assertions
                .assertThat(response)
                .isEqualTo(expectedResponse);
    }
    @Test
    void getSubscriptionPriceAboveTwelveMonthSuccessTest() {
        ResponseDto<Integer> response = makeGet(
                "api/v1/price/price/get?month=12",
                null,
                PRICE_PARAMETERIZED_TYPE_REFERENCE

        );
        log.info("response = {}", response);
        ResponseDto<Integer> expectedResponse = new ResponseDto<>(
                20400,
                StatusType.OK,
                null
        );

        Assertions
                .assertThat(response)
                .isEqualTo(expectedResponse);
    }
}
