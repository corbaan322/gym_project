package com.vlad.repin.gym;


import com.vlad.repin.gym.api.clients.ClientInfo;
import com.vlad.repin.gym.api.clients.GetClientsIdByFullNameRequest;
import com.vlad.repin.gym.api.clients.GetClientsInfoByNameRequest;
import com.vlad.repin.gym.api.clients.UpdateClientInfoRequest;
import com.vlad.repin.gym.domain.ClientEntity;
import com.vlad.repin.gym.domain.ClientsRepository;
import com.vlad.repin.gym.dto.ResponseDto;
import com.vlad.repin.gym.dto.StatusType;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;

import java.util.List;

public class ClientsControllerTest extends GymCommonTest {

    @Autowired
    private ClientsRepository clientsRepository;

    private static final ParameterizedTypeReference<ResponseDto<List<ClientInfo>>>
            LIST_CLIENT_INFO_PARAMETERIZED_TYPE_REFERENCE =
            new ParameterizedTypeReference<>() {
            };

    @Test
    void getAllClientsNameSuccessTest() {
        ResponseDto<List<ClientInfo>> response = makePost("/api/v1/clients/info/get/all",
                null,
                LIST_CLIENT_INFO_PARAMETERIZED_TYPE_REFERENCE);

        ResponseDto<List<ClientInfo>> expectedResult = new ResponseDto<>(
                List.of(
                        new ClientInfo("Victor",
                                "Yakovlef",
                                "89146372699",
                                "5"),
                        new ClientInfo("Yana",
                                "Yakovlefa",
                                "89146372684",
                                "5"),
                        new ClientInfo("John",
                                "Python",
                                "89616378884",
                                "12")
                ),
                StatusType.OK,
                null
        );

        Assertions.assertThat(response.getData().containsAll(expectedResult.getData()))
                .isTrue();
    }

    @Test
    void getClientsInfoByNameSuccessTest() {
        GetClientsInfoByNameRequest request =
                new GetClientsInfoByNameRequest("testNameClientRead");

        ResponseDto<List<ClientInfo>> response = makePost("/api/v1/clients/clients-list-name/get",
                request,
                LIST_CLIENT_INFO_PARAMETERIZED_TYPE_REFERENCE);

        ResponseDto<List<ClientInfo>> expectedResponse = new ResponseDto<>(
                List.of(
                        new ClientInfo(
                                "testNameClientRead",
                                "testLastNameClient",
                                "testPhoneNumberClient",
                                "1")
                ),
                StatusType.OK,
                null
        );

        Assertions
                .assertThat(response)
                .isEqualTo(expectedResponse);
    }

    @Test
    void saveClientInfoSuccessTest() {
        ClientEntity beforeSave = clientsRepository.
                findByFirstNameAndLastName("testName", "testLastName");
        Assertions.assertThat(beforeSave).isNull();

        ClientInfo request = new ClientInfo(
                "testName",
                "testLastName",
                "testPhoneNumber",
                "5"
        );

        makePost("api/v1/clients/info/save",
                request, VOID_PARAMETERIZED_TYPE_REFERENCE);

        ClientEntity expectedResult = new ClientEntity(
                0,
                "testName",
                "testLastName",
                "testPhoneNumber",
                "5"
        );

        ClientEntity afterSave = clientsRepository.
                findByFirstNameAndLastName("testName", "testLastName");

        Assertions.assertThat(afterSave)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(expectedResult);
    }

    @Test
    void deleteClientInfoSuccessTest() {
        ClientInfo clientInfoForDelete = new ClientInfo(
                "testNameForDelete",
                "testLastNameForDelete",
                "testPhoneNumberForDelete",
                "5ForDelete"
        );
        makePost("api/v1/clients/info/save",
                clientInfoForDelete, VOID_PARAMETERIZED_TYPE_REFERENCE);

        GetClientsIdByFullNameRequest request = new GetClientsIdByFullNameRequest(
                "testNameForDelete",
                "testLastNameForDelete"
        );
        makePost("api/v1/clients/info/delete",
                request, VOID_PARAMETERIZED_TYPE_REFERENCE);

        ClientEntity expectedResult =
                clientsRepository.findByFirstNameAndLastName(request.getFirstName(), request.getLastName());

        Assertions.assertThat(expectedResult)
                .isNull();
    }

    @Test
    void updateClientInfoSuccessTest() {
        UpdateClientInfoRequest request = new UpdateClientInfoRequest(
                1000,
                "testNameUPDATE",
                "testLastNameUPDATE",
                "testPhoneNumberUPDATE",
                "5UPDATE"
        );
        ClientEntity beforeUpdate = clientsRepository
                .findByFirstNameAndLastName(
                        request.getFirstName(), request.getLastName()
                );

        Assertions.assertThat(beforeUpdate)
                .isNull();

        makePost("api/v1/clients/info/update",
                request, VOID_PARAMETERIZED_TYPE_REFERENCE);

        ClientEntity afterUpdate = clientsRepository
                .findByFirstNameAndLastName("testNameUPDATE", "testLastNameUPDATE");

        ClientEntity expectedResult = new ClientEntity(
                1000,
                "testNameUPDATE",
                "testLastNameUPDATE",
                "testPhoneNumberUPDATE",
                "5UPDATE"
        );
        Assertions.assertThat(afterUpdate)
                .isEqualTo(expectedResult);
    }
}
