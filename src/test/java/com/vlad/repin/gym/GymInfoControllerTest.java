package com.vlad.repin.gym;

import com.vlad.repin.gym.api.gym.GetGymInfoByNameAndLocationRequest;
import com.vlad.repin.gym.api.gym.GetGymInfoByNameRequest;
import com.vlad.repin.gym.api.gym.GymInfo;
import com.vlad.repin.gym.api.gym.UpdateGymInfoRequest;
import com.vlad.repin.gym.domain.GymInfoEntity;
import com.vlad.repin.gym.domain.GymInfoRepository;
import com.vlad.repin.gym.dto.ResponseDto;
import com.vlad.repin.gym.dto.StatusType;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;

import java.util.List;

public class GymInfoControllerTest extends GymCommonTest {

    @Autowired
    private GymInfoRepository gymInfoRepository;

    private static final ParameterizedTypeReference<ResponseDto<List<GymInfo>>>
            GYM_INFO_LIST_PARAMETERIZED_TYPE_REFERENCE =
            new ParameterizedTypeReference<>() {
            };

    @Test
    void getAllGymsInfoGetSuccessTest() {
        ResponseDto<List<GymInfo>> response = makeGet("/api/v1/gym/test-get-request/get",
                null,
                GYM_INFO_LIST_PARAMETERIZED_TYPE_REFERENCE);

        ResponseDto<List<GymInfo>> expectedResult = new ResponseDto<>(
                List.of(
                        new GymInfo(
                                "GYM FOR REAL MAN",
                                "Moscow, Kutuzovsky Prospekt, 55",
                                "89146379999",
                                "10:00 - 20:00"
                        ),
                        new GymInfo(
                                "Gold`s Gym",
                                "USA, 360 Hampton Dr, Venice, CA 90291",
                                "+13103926004",
                                "05:30-23:00"
                        )),
                StatusType.OK,
                null
        );

        Assertions
                .assertThat(response.getData().containsAll(expectedResult.getData()))
                .isTrue();
    }

    @Test
    void getGymInfoListSuccessTest() {
        ResponseDto<List<GymInfo>> response = makePost("/api/v1/gym/info/get/all",
                null,
                GYM_INFO_LIST_PARAMETERIZED_TYPE_REFERENCE);

        ResponseDto<List<GymInfo>> expectedResult = new ResponseDto<>(
                List.of(
                        new GymInfo(
                                "GYM FOR REAL MAN",
                                "Moscow, Kutuzovsky Prospekt, 55",
                                "89146379999",
                                "10:00 - 20:00"
                        ),
                        new GymInfo(
                                "Gold`s Gym",
                                "USA, 360 Hampton Dr, Venice, CA 90291",
                                "+13103926004",
                                "05:30-23:00"
                        )),
                StatusType.OK,
                null
        );

        Assertions
                .assertThat(response.getData().containsAll(expectedResult.getData()))
                .isTrue();
    }

    @Test
    void getGymInfoByNameSuccessTest() {
        GetGymInfoByNameRequest request =
                new GetGymInfoByNameRequest("testGymNameGymInfoRead");

        ResponseDto<List<GymInfo>> response = makePost(
                "/api/v1/gym/gym-list-name/get",
                request,
                GYM_INFO_LIST_PARAMETERIZED_TYPE_REFERENCE);

        ResponseDto<List<GymInfo>> expectedResult = new ResponseDto<>(
                List.of(
                        new GymInfo("testGymNameGymInfoRead",
                                "testLocationGymInfo",
                                "testPhoneNumberGymInfo",
                                "10:00 - 20:00")
                ),
                StatusType.OK,
                null
        );
        Assertions
                .assertThat(response)
                .isEqualTo(expectedResult);
    }

    @Test
    void saveGymInfoSuccessTest() {
        GymInfoEntity beforeSave = gymInfoRepository.findByGymNameAndLocation(
                "testName", "testLocation");
        Assertions.assertThat(beforeSave).isNull();

        GymInfo request = new GymInfo(
                "testName",
                "testLocation",
                "testNumber",
                "testHours"
        );

        makePost("/api/v1/gym/info/save",
                request, VOID_PARAMETERIZED_TYPE_REFERENCE);

        GymInfoEntity expectedResult = new GymInfoEntity(
                0,
                "testName",
                "testLocation",
                "testNumber",
                "testHours"
        );

        GymInfoEntity afterSave = gymInfoRepository.findByGymNameAndLocation(
                "testName", "testLocation");
        Assertions
                .assertThat(afterSave)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(expectedResult);
    }

    @Test
    void deleteGymInfoSuccessTest() {
        GymInfo gymInfoForDelete = new GymInfo(
                "testNameDelete",
                "testLocationDelete",
                "testNumberDelete",
                "testHoursDelete"
        );
        makePost("/api/v1/gym/info/save",
                gymInfoForDelete, VOID_PARAMETERIZED_TYPE_REFERENCE);

        GetGymInfoByNameAndLocationRequest request = new GetGymInfoByNameAndLocationRequest(
                "testNameDelete",
                "testLocationDelete");
        makePost("/api/v1/gym/info/delete",
                request, VOID_PARAMETERIZED_TYPE_REFERENCE);

        GymInfoEntity expectedResult = gymInfoRepository.findByGymNameAndLocation(
                request.getGymName(), request.getLocation());

        Assertions.assertThat(expectedResult)
                .isNull();
    }

    @Test
    void updateGymInfoSuccessTest() {
        UpdateGymInfoRequest request = new UpdateGymInfoRequest(
                1000,
                "testNameUPDATE",
                "testLocationUPDATE",
                "testNumberUPDATE",
                "testHoursUPDATE"
        );
        GymInfoEntity beforeUpdate = gymInfoRepository
                .findByGymNameAndLocation(request.getGymName(), request.getLocation());

        Assertions.assertThat(beforeUpdate)
                        .isNull();

        makePost("/api/v1/gym/info/update",
                request, VOID_PARAMETERIZED_TYPE_REFERENCE);

        GymInfoEntity afterUpdate = gymInfoRepository
                .findByGymNameAndLocation(request.getGymName(), request.getLocation());

        GymInfoEntity expectedResult = new GymInfoEntity(
                1000,
                "testNameUPDATE",
                "testLocationUPDATE",
                "testNumberUPDATE",
                "testHoursUPDATE"
        );
        Assertions.assertThat(afterUpdate)
                .isEqualTo(expectedResult);
    }
}
