package com.vlad.repin.gym;


import com.vlad.repin.gym.api.test.*;
import com.vlad.repin.gym.domain.TestEntity;
import com.vlad.repin.gym.domain.TestDataRepository;
import com.vlad.repin.gym.dto.ResponseDto;
import com.vlad.repin.gym.dto.StatusType;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;


import java.time.LocalDateTime;
import java.util.List;

public class TestControllerTest extends GymCommonTest {

    @Autowired
    private TestDataRepository testRepository;

    private static final ParameterizedTypeReference<ResponseDto<List<TestData>>>
            TEST_DATA_LIST_PARAMETERIZED_TYPE_REFERENCE =
            new ParameterizedTypeReference<>() {
            };

    @Test
    void getTestDataListSuccessTest() {
        ResponseDto<List<TestData>> response = makePost("/api/v1/test/data/all",
                null,
                TEST_DATA_LIST_PARAMETERIZED_TYPE_REFERENCE);

        ResponseDto<List<TestData>> expectedResult = new ResponseDto<>(
                List.of(
                        new TestData("Kek",
                                LocalDateTime.parse("2016-11-09T03:44:44.797000"),
                                TestType.ONE),
                        new TestData("LOL",
                                LocalDateTime.parse("2016-11-09T03:44:44.797000"),
                                TestType.ONE)
                ),
                StatusType.OK,
                null
        );

        Assertions
                .assertThat(response.getData().containsAll(expectedResult.getData()))
                .isTrue();
    }

    @Test
    void getTestDataByNameSuccessTest() {
        GetTestDataByNameRequest request =
                new GetTestDataByNameRequest("testTestNameRead");

        ResponseDto<List<TestData>> response = makePost("api/v1/test/data/get",
                request,
                TEST_DATA_LIST_PARAMETERIZED_TYPE_REFERENCE
        );

        ResponseDto<List<TestData>> expectedResult = new ResponseDto<>(
                List.of(
                        new TestData("testTestNameRead",
                                LocalDateTime.parse("2016-11-09T03:44:44.797000"),
                                TestType.ONE)
                ), StatusType.OK,
                null
        );
        Assertions
                .assertThat(response)
                .isEqualTo(expectedResult);
    }


    @Test
    void saveTestDataSuccessTest() {
        TestEntity beforeSave = testRepository.findByNameAndTestType("testName", TestType.ONE);
        Assertions.assertThat(beforeSave).isNull();

        TestData request = new TestData(
                "testName",
                LocalDateTime.parse("2016-11-09T00:44:44.797000"),
                TestType.ONE);

        makePost("/api/v1/test/data/save",
                request, VOID_PARAMETERIZED_TYPE_REFERENCE);

        TestEntity expectedResult = new TestEntity(
                0,
                "testName",
                LocalDateTime.parse("2016-11-09T00:44:44.797000"),
                TestType.ONE);

        TestEntity afterSave = testRepository.findByNameAndTestType("testName", TestType.ONE);
        Assertions
                .assertThat(afterSave)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(expectedResult);
    }

    @Test
    void deleteTestDataSuccessTest() {
        TestData testDataForDelete = new TestData(
                "testNameForDelete",
                LocalDateTime.parse("2016-11-09T00:44:44.797000"),
                TestType.ONE
        );
        makePost("/api/v1/test/data/save",
                testDataForDelete, VOID_PARAMETERIZED_TYPE_REFERENCE);

        GetTestDataByNameAndTypeRequest request = new GetTestDataByNameAndTypeRequest(
                "testNameForDelete",
                TestType.ONE
        );
        makePost("/api/v1/test/data/delete",
                request, VOID_PARAMETERIZED_TYPE_REFERENCE);
        TestEntity expectedResult = testRepository.findByNameAndTestType(
                request.getName(), request.getTestType()
        );
        Assertions.assertThat(expectedResult)
                .isNull();
    }

    @Test
    void updateTestDataSuccessTest() {
        UpdateTestDataRequest request = new UpdateTestDataRequest(
                1000,
                "testTestNameUpdate",
                LocalDateTime.parse("2016-11-09T03:44:44.797000"),
                TestType.ONE
        );
        TestEntity beforeUpdate = testRepository
                .findByNameAndTestType(request.getName(), request.getTestType());

        Assertions.assertThat(beforeUpdate)
                .isNull();

        makePost("/api/v1/test/data/update",
                request, VOID_PARAMETERIZED_TYPE_REFERENCE);
        TestEntity afterUpdate = testRepository
                .findByNameAndTestType(request.getName(), request.getTestType());

        TestEntity expectedResult = new TestEntity(
                1000,
                "testTestNameUpdate",
                LocalDateTime.parse("2016-11-09T03:44:44.797000"),
                TestType.ONE
        );
        Assertions.assertThat(afterUpdate)
                .isEqualTo(expectedResult);
    }
}
