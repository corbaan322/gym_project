package com.vlad.repin.gym;

import org.assertj.core.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@ActiveProfiles("test")
@ContextConfiguration(initializers = PostgresInDockerInitializer.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GymCommonTest {
    public static final ParameterizedTypeReference<Void>
            VOID_PARAMETERIZED_TYPE_REFERENCE =
            new ParameterizedTypeReference<>() {
            };

    @LocalServerPort
    public int serverPort;

    @Autowired
    private TestRestTemplate restTemplate;

    public <T, R> T makePost(
            String url,
            R request,
            ParameterizedTypeReference<T> responseType
    ) {
        HttpEntity<R> httpEntity = new HttpEntity<>(request);

        ResponseEntity<T> response = restTemplate.exchange(
                getFullUrl(url),
                HttpMethod.POST,
                httpEntity,
                responseType
        );

        Assertions
                .assertThat(response.getStatusCode())
                .isEqualTo(HttpStatus.OK);

        return response.getBody();
    }

    public <T, R> T makeGet(
            String url,
            R request,
            ParameterizedTypeReference<T> responseType
    ) {
        HttpEntity<R> httpEntity = new HttpEntity<>(request);

        ResponseEntity<T> response = restTemplate.exchange(
                getFullUrl(url),
                HttpMethod.GET,
                httpEntity,
                responseType
        );

        Assertions
                .assertThat(response.getStatusCode())
                .isEqualTo(HttpStatus.OK);

        return response.getBody();
    }



    public String getFullUrl(String urlTail) {
        return "http://localhost:" + serverPort + "/" + urlTail;
    }
}
