package com.vlad.repin.gym;


import com.vlad.repin.gym.api.instructors.GetInstructorInfoByFullNameRequest;
import com.vlad.repin.gym.api.instructors.GetInstructorsInfoByNameRequest;
import com.vlad.repin.gym.api.instructors.InstructorInfo;
import com.vlad.repin.gym.api.instructors.UpdateInstructorInfoRequest;
import com.vlad.repin.gym.domain.InstructorsEntity;
import com.vlad.repin.gym.domain.InstructorsRepository;
import com.vlad.repin.gym.dto.ResponseDto;
import com.vlad.repin.gym.dto.StatusType;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;

import java.util.List;

public class InstructorsControllerTest extends GymCommonTest {

    @Autowired
    private InstructorsRepository repository;
    private static final ParameterizedTypeReference<ResponseDto<List<InstructorInfo>>>
            INSTRUCTOR_INFO_LIST_PARAMETERIZED_TYPE_REFERENCE =
            new ParameterizedTypeReference<>() {
            };

    @Test
    void getInstructorsInfoListSuccessTest() {
        ResponseDto<List<InstructorInfo>> response = makePost(
                "/api/v1/instructors/get/all",
                null,
                INSTRUCTOR_INFO_LIST_PARAMETERIZED_TYPE_REFERENCE);

        ResponseDto<List<InstructorInfo>> expectedResult = new ResponseDto<>(
                List.of(
                        new InstructorInfo(
                                "Oleg",
                                "Smirnof",
                                10
                        ),
                        new InstructorInfo(
                                "Maria",
                                "Grishina",
                                5
                        ),
                        new InstructorInfo(
                                "Sergei",
                                "Bolotov",
                                1
                        )
                ),
                StatusType.OK,
                null
        );
        Assertions
                .assertThat(response.getData().containsAll(expectedResult.getData()))
                .isTrue();
    }

    @Test
    void getInstructorsInfoByNameSuccessTest() {
        GetInstructorsInfoByNameRequest request =
                new GetInstructorsInfoByNameRequest("testNameInstructorRead");

        ResponseDto<List<InstructorInfo>> response = makePost("/api/v1/instructors/info/get",
                request,
                INSTRUCTOR_INFO_LIST_PARAMETERIZED_TYPE_REFERENCE);

        ResponseDto<List<InstructorInfo>> expectedResponse = new ResponseDto<>(
                List.of(
                        new InstructorInfo(
                                "testNameInstructorRead",
                                "testLastNameInstructor",
                                10)
                ), StatusType.OK,
                null
        );

        Assertions.assertThat(response)
                .isEqualTo(expectedResponse);
    }

    @Test
    void saveInstructorSuccessTest() {
        InstructorsEntity beforeSave = repository.findByFirstNameAndLastName(
                "testName", "testLastName");
        Assertions.assertThat(beforeSave).isNull();

        InstructorInfo request = new InstructorInfo(
                "testName",
                "testLastName",
                1
        );

        makePost("/api/v1/instructors/info/save",
                request, VOID_PARAMETERIZED_TYPE_REFERENCE);

        InstructorsEntity expectedResult = new InstructorsEntity(
                0,
                "testName",
                "testLastName",
                1
        );

        InstructorsEntity afterSave = repository
                .findByFirstNameAndLastName("testName", "testLastName");

        Assertions
                .assertThat(afterSave)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(expectedResult);
    }

    @Test
    void deleteInstructorInfoSuccessTest() {
        InstructorInfo instructorInfoForDelete = new InstructorInfo(
                "testNameForDelete",
                "testLastNameForDelete",
                1
        );
        makePost("/api/v1/instructors/info/save",
                instructorInfoForDelete, VOID_PARAMETERIZED_TYPE_REFERENCE);
        GetInstructorInfoByFullNameRequest request = new GetInstructorInfoByFullNameRequest(
                "testNameForDelete",
                "testLastNameForDelete"
        );
        makePost("/api/v1/instructors/info/delete",
                request, VOID_PARAMETERIZED_TYPE_REFERENCE);

        InstructorsEntity expectedResult = repository
                .findByFirstNameAndLastName(request.getFirstName(), request.getLastName());

        Assertions.assertThat(expectedResult)
                .isNull();

    }

    @Test
    void updateInstructorInfoSuccessTest() {
        UpdateInstructorInfoRequest request = new UpdateInstructorInfoRequest(
                1000,
                "testNameInstructorUpdate",
                "testNameInstructorUpdate",
                1
        );
        InstructorsEntity beforeUpdate = repository
                .findByFirstNameAndLastName(
                        request.getFirstName(), request.getLastName()
                );
        Assertions.assertThat(beforeUpdate)
                .isNull();

        makePost("/api/v1/instructors/info/update",
                request, VOID_PARAMETERIZED_TYPE_REFERENCE);
        InstructorsEntity afterUpdate = repository.
                findByFirstNameAndLastName("testNameInstructorUpdate",
                        "testNameInstructorUpdate");
        InstructorsEntity expectedResult = new InstructorsEntity(
                1000,
                "testNameInstructorUpdate",
                "testNameInstructorUpdate",
                1
        );
        Assertions.assertThat(afterUpdate)
                .isEqualTo(expectedResult);
    }
}
