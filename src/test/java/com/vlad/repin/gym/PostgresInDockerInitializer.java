package com.vlad.repin.gym;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;

import java.io.Serial;

/**
 * Стартует Postgres в Docker'е, меняет настройки тестируемого Spring Boot приложения, чтобы работало с этой Postgres.
 */
@Slf4j
class PostgresInDockerInitializer
        implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final String POSTGRES_IMAGE = "postgres:13-alpine";

    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        if (!Boolean.parseBoolean(
                configurableApplicationContext.getEnvironment().getProperty("test.postgres.enabled", "false"))
        ) {
            log.info("initialize: Postgres for tests is disabled");
            return;
        }
        log.info("initialize: Starting Postgres for tests from \"{}\"...", POSTGRES_IMAGE);
        try {
            PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>(POSTGRES_IMAGE);
            postgreSQLContainer.start();
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword(),
                    "spring.datasource.port=" + postgreSQLContainer.getFirstMappedPort(),
                    "spring.datasource.driverClassName=org.postgresql.Driver"
            ).applyTo(configurableApplicationContext.getEnvironment());
            log.info("initialize: Successfully started Postgres for tests from \"{}\": {}", postgreSQLContainer.getDockerImageName(),
                    postgreSQLContainer.getJdbcUrl());
        } catch (Exception ex) {
            log.error("Failed to start Postgres for tests.", ex);
            throw new PostgresStartException(ex);
        }
    }

    static class PostgresStartException extends RuntimeException {

        @Serial
        private static final long serialVersionUID = 5136791096656576763L;

        public PostgresStartException(Throwable cause) {
            super(cause);
        }
    }
}