package com.vlad.repin.gym.controller;


import com.vlad.repin.gym.api.test.GetTestDataByNameAndTypeRequest;
import com.vlad.repin.gym.api.test.GetTestDataByNameRequest;
import com.vlad.repin.gym.api.test.TestData;
import com.vlad.repin.gym.api.test.UpdateTestDataRequest;
import com.vlad.repin.gym.dto.ResponseDto;
import com.vlad.repin.gym.exeptions.EntryDoesNotExistException;
import com.vlad.repin.gym.service.TestService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/test")
@Slf4j
@RequiredArgsConstructor
public class TestController {
    private final TestService testService;

    @PostMapping("data/save")
    public ResponseDto<Void> saveTestData(@RequestBody TestData request) {
        log.info("Incoming request {}", request);
        testService.saveTestData(request);
        log.info("Saved successfully");
        return ResponseDto.success();
    }

    @PostMapping("data/all")
    public ResponseDto<List<TestData>> getAllTestData() {
        log.info("Incoming getAllTestData request");
        List<TestData> testDataList = testService.getAllTestData();
        log.info("testDataList = {}", testDataList);
        return ResponseDto.success(testDataList);
    }

    @PostMapping("data/get")
    public ResponseDto<List<TestData>> getTestName(@RequestBody GetTestDataByNameRequest request)
            throws EntryDoesNotExistException {
        log.info("Incoming request {}", request);
        List<TestData> testDataList = testService.getListTestDataByName(request.getName());
        log.info("testDataList = {}", testDataList);
        return ResponseDto.success(testDataList);
    }


    @PostMapping("data/update")
    public ResponseDto<Void> updateClientInfo(@RequestBody UpdateTestDataRequest request)
            throws EntryDoesNotExistException {
        log.info("updateRequest = {}", request);
        testService.updateTestData(request);
        log.info("Update successfully");
        return ResponseDto.success();
    }

    @PostMapping("data/delete")
    public ResponseDto<Void> delete(@RequestBody GetTestDataByNameAndTypeRequest request)
            throws EntryDoesNotExistException {
        log.info("Incoming request {}", request);
        testService.delete(request.getName(), request.getTestType());
        log.info("Delete successfully");
        return ResponseDto.success();
    }
}
