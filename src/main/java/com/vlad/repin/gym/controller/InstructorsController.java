package com.vlad.repin.gym.controller;


import com.vlad.repin.gym.api.instructors.GetInstructorInfoByFullNameRequest;
import com.vlad.repin.gym.api.instructors.GetInstructorsInfoByNameRequest;
import com.vlad.repin.gym.api.instructors.InstructorInfo;
import com.vlad.repin.gym.api.instructors.UpdateInstructorInfoRequest;
import com.vlad.repin.gym.dto.ResponseDto;
import com.vlad.repin.gym.exeptions.EntryDoesNotExistException;
import com.vlad.repin.gym.service.InstructorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v1/instructors")
@Slf4j
@RequiredArgsConstructor
public class InstructorsController {
    private final InstructorService instructorService;

    @PostMapping("info/save")
    public ResponseDto<Void> saveInstructorInfo(@RequestBody InstructorInfo request) {
        log.info("Incoming request = {}", request);
        instructorService.saveInstructorData(request);
        log.info("Saved successfully");
        return ResponseDto.success();
    }

    @PostMapping("get/all")
    public ResponseDto<List<InstructorInfo>> getAllInstructorName() {
        log.info("Incoming getAllInstructorName");
        List<InstructorInfo> instructorInfoList = instructorService.getAllInstructorNames();
        log.info("instructorInfoList = {}", instructorInfoList);
        return ResponseDto.success(instructorInfoList);
    }

    @PostMapping("id/get")
    public ResponseDto<String> getInstructorIdByName(
            @RequestBody GetInstructorInfoByFullNameRequest request)
            throws EntryDoesNotExistException {
        log.info("Incoming request  = {} ", request);
        String instructorInfoByName = instructorService
                .getInstructorIdByName(request.getFirstName(), request.getLastName());
        log.info("Incoming instructorInfoByName  = {} ", instructorInfoByName);
        return ResponseDto.success(instructorInfoByName);
    }

    @PostMapping("info/get")
    public ResponseDto<List<InstructorInfo>> getInstructorName(
            @RequestBody GetInstructorsInfoByNameRequest request)
            throws EntryDoesNotExistException {
        log.info("Incoming request  = {} ", request);
        List<InstructorInfo> instructorInfoList = instructorService.
                getInstructorDataByName(request.getFirstName());
        log.info("instructorInfoList = {}", instructorInfoList);
        return ResponseDto.success(instructorInfoList);
    }

    @PostMapping("info/update")
    public ResponseDto<Void> updateClientInfo(@RequestBody UpdateInstructorInfoRequest request)
            throws EntryDoesNotExistException {
        log.info("updateRequest = {}", request);
        instructorService.updateInstructorInfo(request);
        log.info("Update successfully");
        return ResponseDto.success();
    }

    @PostMapping("info/delete")
    public ResponseDto<Void> deleteInstructorInfo(@RequestBody GetInstructorInfoByFullNameRequest request)
            throws EntryDoesNotExistException {
        log.info("Incoming request = {}", request);
        instructorService.deleteInstructorInfo
                (request.getFirstName(), request.getLastName());
        log.info("Delete successfully");
        return ResponseDto.success();
    }


}
