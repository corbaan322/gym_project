package com.vlad.repin.gym.controller;


import com.vlad.repin.gym.annotation.GymBaseExceptionHandler;
import com.vlad.repin.gym.api.clients.ClientInfo;
import com.vlad.repin.gym.api.clients.GetClientsIdByFullNameRequest;
import com.vlad.repin.gym.api.clients.GetClientsInfoByNameRequest;
import com.vlad.repin.gym.api.clients.UpdateClientInfoRequest;
import com.vlad.repin.gym.dto.ResponseDto;
import com.vlad.repin.gym.exeptions.EntryDoesNotExistException;
import com.vlad.repin.gym.service.ClientsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v1/clients")
@Slf4j
@RequiredArgsConstructor
@GymBaseExceptionHandler
public class ClientsController {
    private final ClientsService clientsService;

    @PostMapping("info/save")
    public ResponseDto<Void> saveClientsInfo(@RequestBody ClientInfo request) {
        log.info("Incoming request {}", request);
        clientsService.saveClientsInfo(request);
        log.info("Saved successfully");
        return ResponseDto.success();
    }

    @PostMapping("info/get/all")
    public ResponseDto<List<ClientInfo>> getAllClientsName() {
        log.info("Incoming getAllClientsName");
        List<ClientInfo> clientInfoList = clientsService.getAllClientsNames();
        log.info("clientInfoList = {}", clientInfoList);
        return ResponseDto.success(clientInfoList);
    }

    @PostMapping("id/get")
    public ResponseDto<String> getClientsIdByName(@RequestBody GetClientsIdByFullNameRequest request)
            throws EntryDoesNotExistException {
        log.info("Incoming request  = {} ", request);
        String clientInfoByName = clientsService.
                getClientIdByName(request.getFirstName(), request.getLastName());
        log.info("clientInfoByName  = {} ", clientInfoByName);
        return ResponseDto.success(clientInfoByName);
    }

    @PostMapping("clients-list-name/get")
    public ResponseDto<List<ClientInfo>> getClientsInfoByName(@RequestBody GetClientsInfoByNameRequest request)
            throws EntryDoesNotExistException {
        log.info("Incoming request  = {} ", request);
        List<ClientInfo> clientInfoList = clientsService.getListClientsInfoByName(request.getFirstName());
        log.info("clientInfoList  = {} ", clientInfoList);
        return ResponseDto.success(clientInfoList);
    }


    @PostMapping("info/update")
    public ResponseDto<Void> updateClientInfo
            (@RequestBody UpdateClientInfoRequest request) throws EntryDoesNotExistException {
        log.info("updateRequest = {}", request);
        clientsService.updateClientInfo(request);
        log.info("Update successfully");
        return ResponseDto.success();
    }

    @PostMapping("info/delete")
    public ResponseDto<Void> deleteClientsInfo(@RequestBody GetClientsIdByFullNameRequest request)
            throws EntryDoesNotExistException {
        log.info("Incoming request {}", request);
        clientsService.delete(request.getFirstName(), request.getLastName());
        log.info("Delete successfully");
        return ResponseDto.success();
    }
}
