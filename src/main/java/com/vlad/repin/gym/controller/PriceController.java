package com.vlad.repin.gym.controller;

import com.vlad.repin.gym.dto.ResponseDto;
import com.vlad.repin.gym.service.PriceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/price")
@Slf4j
@RequiredArgsConstructor
public class PriceController {
    private final PriceService priceService;

    @GetMapping("price/get")
    public ResponseDto<Integer> getSubscriptionPrice(@RequestParam(name = "month") int month) {
        log.info("Incoming params: {}", month);
        Integer subscriptionPrice = priceService.getSubscriptionPrice(month);
        log.info("subscriptionPrice = {}", subscriptionPrice);
        return ResponseDto.success(subscriptionPrice);
    }
}
