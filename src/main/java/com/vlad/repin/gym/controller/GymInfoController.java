package com.vlad.repin.gym.controller;


import com.vlad.repin.gym.api.gym.GetGymInfoByNameAndLocationRequest;
import com.vlad.repin.gym.api.gym.GetGymInfoByNameRequest;
import com.vlad.repin.gym.api.gym.GymInfo;
import com.vlad.repin.gym.api.gym.UpdateGymInfoRequest;
import com.vlad.repin.gym.dto.ResponseDto;
import com.vlad.repin.gym.exeptions.EntryDoesNotExistException;
import com.vlad.repin.gym.service.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/gym")
@Slf4j
@RequiredArgsConstructor
public class GymInfoController {
    private final GymInfoService gymInfoService;

    @PostMapping("info/save")
    public ResponseDto<Void> saveGymInfo(@RequestBody GymInfo request) {
        log.info("Incoming request {}", request);
        gymInfoService.saveGymInfo(request);
        log.info("Saved successfully");
        return ResponseDto.success();
    }

    @PostMapping("info/get/all")
    public ResponseDto<List<GymInfo>> getAllGymsInfo() {
        log.info("Incoming getGymInfo");
        List<GymInfo> gymInfoList = gymInfoService.getAllGymsInfo();
        log.info("gymInfoList = {}", gymInfoList);
        return ResponseDto.success(gymInfoList);
    }

    @PostMapping("gym-list-name/get")
    public ResponseDto<List<GymInfo>> getGymsInfoByName(@RequestBody GetGymInfoByNameRequest request)
            throws EntryDoesNotExistException {
        log.info("Incoming request  = {} ", request);
        List<GymInfo> gymInfoList = gymInfoService.getListGymInfoByName(request.getGymName());
        log.info("gymInfoList = {}", gymInfoList);
        return ResponseDto.success(gymInfoList);
    }

    @PostMapping("info/update")
    public ResponseDto<Void> updateClientInfo
            (@RequestBody UpdateGymInfoRequest request) throws EntryDoesNotExistException {
        log.info("updateRequest = {}", request);
        gymInfoService.updateGymInfo(request);
        log.info("Update successfully");
        return ResponseDto.success();
    }

    @PostMapping("info/delete")
    public ResponseDto<Void> deleteGymInfo(@RequestBody GetGymInfoByNameAndLocationRequest request)
            throws EntryDoesNotExistException {
        log.info("Incoming request - {}", request);
        gymInfoService.deleteGymInfo(request.getGymName(), request.getLocation());
        log.info("Delete successfully");
        return ResponseDto.success();
    }

    @GetMapping("open-now/get")
    public ResponseDto<Boolean> getOpenNow() {
        log.info("Incoming getOpenNow");
        Boolean openNowBool = gymInfoService.chekOpenGymNow();
        log.info("openNowBool = {}", openNowBool);
        return ResponseDto.success(openNowBool);
    }

    // Пробный метод для работы теста через GET запрос
    @GetMapping("test-get-request/get")
    public ResponseDto<List<GymInfo>> getAllGymsInfoGet() {
        log.info("Incoming getAllGymsInfoGet");
        log.info("Incoming getGymInfo");
        List<GymInfo> gymInfoList = gymInfoService.getAllGymsInfo();
        log.info("gymInfoList = {}", gymInfoList);
        return ResponseDto.success(gymInfoList);
    }
}
