package com.vlad.repin.gym.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "gym_info")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GymInfoEntity {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    private String gymName;
    private String location;
    private String phoneNumber;
    private String openingHours;
}
