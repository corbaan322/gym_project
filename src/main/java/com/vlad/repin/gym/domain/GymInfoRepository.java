package com.vlad.repin.gym.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GymInfoRepository extends JpaRepository<GymInfoEntity, Integer> {
    List<GymInfoEntity> findByGymName(String name);

    GymInfoEntity findByGymNameAndLocation(String gymName, String location);

    GymInfoEntity findById(int id);
}
