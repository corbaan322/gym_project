package com.vlad.repin.gym.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InstructorsRepository extends JpaRepository<InstructorsEntity, Integer> {
    List<InstructorsEntity> findByFirstName(String name);

    InstructorsEntity findByFirstNameAndLastName(String firstName, String lastName);

    InstructorsEntity findById(int id);
}
