package com.vlad.repin.gym.domain;

import com.vlad.repin.gym.api.test.TestType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestDataRepository extends JpaRepository<TestEntity, Integer> {
    List<TestEntity> findByName(String name);

    TestEntity findByNameAndTestType(String name, TestType testType);

    TestEntity findById(int id);
}
