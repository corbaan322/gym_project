package com.vlad.repin.gym.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientsRepository extends JpaRepository<ClientEntity, Integer> {
    List<ClientEntity> findByFirstName(String firstName);

    ClientEntity findByFirstNameAndLastName(String firstName, String lastName);

    ClientEntity findById(int id);
}
