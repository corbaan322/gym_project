package com.vlad.repin.gym.exeptions;

public class EntryDoesNotExistException extends RuntimeException {
    public EntryDoesNotExistException(String message) {
        super(message);
    }
}
