package com.vlad.repin.gym.advice;

import com.vlad.repin.gym.annotation.GymBaseExceptionHandler;
import com.vlad.repin.gym.dto.ResponseDto;
import com.vlad.repin.gym.exeptions.EntryDoesNotExistException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
@Slf4j
@ControllerAdvice(annotations = GymBaseExceptionHandler.class)
public class GymBaseAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler({EntryDoesNotExistException.class, IllegalArgumentException.class})
    public ResponseEntity<ResponseDto<Exception>> handleException(Exception exc) {
        String message = exc.getMessage();
        log.info("ERROR, message = {}", message);
        return new ResponseEntity<>(ResponseDto.error(message),HttpStatus.OK);
    }
}
