package com.vlad.repin.gym.service;


import com.vlad.repin.gym.api.clients.ClientInfo;
import com.vlad.repin.gym.api.clients.UpdateClientInfoRequest;
import com.vlad.repin.gym.domain.ClientEntity;
import com.vlad.repin.gym.domain.ClientsRepository;
import com.vlad.repin.gym.exeptions.EntryDoesNotExistException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ClientsService {
    private final ClientsRepository clientsRepository;

    public void saveClientsInfo(ClientInfo request) {
        log.debug("Incoming request = {}", request);

        ClientEntity.ClientEntityBuilder builder = ClientEntity.builder();
        builder.firstName(request.getFirstName())
                .lastName(request.getLastName())
                .phoneNumber(request.getPhoneNumber())
                .paidMonths(request.getPaidMonths());
        ClientEntity entity = builder.build();

        clientsRepository.save(entity);
        log.debug("Save success");
    }

    private ClientInfo toClientsInfo(ClientEntity clientEntity) {
        log.debug("clientEntity = {}", clientEntity);
        return new ClientInfo(
                clientEntity.getFirstName(),
                clientEntity.getLastName(),
                clientEntity.getPhoneNumber(),
                clientEntity.getPaidMonths()
        );
    }

    public List<ClientInfo> getAllClientsNames() {
        List<ClientEntity> clientsDataList = clientsRepository.findAll();
        log.debug("clientsDataList = {}", clientsDataList);
        return clientsDataList.stream()
                .map(this::toClientsInfo)
                .toList();
    }

    public String getClientIdByName(String firstName, String lastName)
            throws EntryDoesNotExistException {
        ClientEntity clientEntity = clientsRepository.findByFirstNameAndLastName(firstName, lastName);
        if (clientEntity == null) throw new EntryDoesNotExistException("Entry does not exist");
        log.debug("clientEntity = {}", clientEntity);
        return clientEntity.getId().toString();
    }

    public List<ClientInfo> getListClientsInfoByName(String firstName)
            throws EntryDoesNotExistException {
        List<ClientEntity> clientEntityList = clientsRepository.findByFirstName(firstName);
        if (clientEntityList == null) throw new EntryDoesNotExistException("Entry does not exist");
        log.debug("clientEntityList = {}", clientEntityList);
        return clientEntityList.stream()
                .map(this::toClientsInfo)
                .toList();
    }

    public void updateClientInfo(UpdateClientInfoRequest request) throws EntryDoesNotExistException {
        ClientEntity clientEntity = clientsRepository.findById(request.getId());
        if (clientEntity == null) throw new
                EntryDoesNotExistException("Entry does not exist");

        clientEntity.setFirstName(
                request.getFirstName() != null ? request.getFirstName() : clientEntity.getFirstName()
        );
        clientEntity.setLastName(
                request.getLastName() != null ? request.getLastName() : clientEntity.getLastName()
        );
        clientEntity.setPhoneNumber(
                request.getPhoneNumber() != null ? request.getPhoneNumber() : clientEntity.getPhoneNumber()
        );
        clientEntity.setPaidMonths(
                request.getPaidMonths() != null ? request.getPaidMonths() : clientEntity.getPaidMonths()
        );

        clientsRepository.save(clientEntity);
        log.debug("Update success");
    }

    public void delete(String firstName, String lastName) throws EntryDoesNotExistException {
        ClientEntity clientEntity = clientsRepository.findByFirstNameAndLastName(firstName, lastName);
        if (clientEntity == null) throw new EntryDoesNotExistException("Entry does not exist");

        log.debug("clientEntity = {}", clientEntity);
        clientsRepository.delete(clientEntity);
        log.debug("Delete success");
    }
}
