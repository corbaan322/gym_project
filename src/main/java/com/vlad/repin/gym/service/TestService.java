package com.vlad.repin.gym.service;

import com.vlad.repin.gym.api.test.TestData;
import com.vlad.repin.gym.api.test.TestType;
import com.vlad.repin.gym.api.test.UpdateTestDataRequest;
import com.vlad.repin.gym.domain.TestEntity;
import com.vlad.repin.gym.domain.TestDataRepository;
import com.vlad.repin.gym.exeptions.EntryDoesNotExistException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class TestService {
    private final TestDataRepository testDataRepository;

    public void saveTestData(TestData request) {
        log.debug("Incoming request = {}", request);
        TestEntity.TestEntityBuilder builder = TestEntity.builder();
        builder.name(request.getName())
                .localDateTime(request.getLocalDateTime())
                .testType(request.getTestType())
                .build();
        TestEntity entity = builder.build();
        testDataRepository.save(entity);
        log.debug("Save success");
    }

    private TestData toTestData(TestEntity testEntity) {
        return new TestData(
                testEntity.getName(),
                testEntity.getLocalDateTime(),
                testEntity.getTestType()
        );
    }

    public List<TestData> getAllTestData() {
        List<TestEntity> testDataList = testDataRepository.findAll();
        return testDataList.stream()
                .map(this::toTestData)
                .toList();
    }

    public List<TestData> getListTestDataByName(String name)
            throws EntryDoesNotExistException {
        log.debug("name = {}", name);
        List<TestEntity> testEntityList = testDataRepository.findByName(name);
        if (testEntityList == null) throw new EntryDoesNotExistException("EntryDoesNotExistException");
        log.debug("testEntityList = {}", testEntityList);
        return testEntityList.stream()
                .map(this::toTestData)
                .toList();
    }

    public void updateTestData(UpdateTestDataRequest request)
            throws EntryDoesNotExistException {
        TestEntity testEntity = testDataRepository.findById(request.getId());
        if (testEntity == null) throw new EntryDoesNotExistException("EntryDoesNotExistException");

        testEntity.setName(request.getName() != null ? request.getName() : testEntity.getName());
        testEntity.setLocalDateTime(request.getLocalDateTime() != null ?
                request.getLocalDateTime() : testEntity.getLocalDateTime());
        testEntity.setTestType(request.getTestType() != null ?
                request.getTestType() : testEntity.getTestType());

        testDataRepository.save(testEntity);
        log.debug("Update success");
    }

    public void delete(String name, TestType testType) {
        log.debug("name = {}, testType = {}", name, testType);
        TestEntity testEntity = testDataRepository.findByNameAndTestType(name, testType);
        log.debug("testEntity = {}", testEntity);
        testDataRepository.delete(testEntity);
        log.debug("Delete success");
    }
}
