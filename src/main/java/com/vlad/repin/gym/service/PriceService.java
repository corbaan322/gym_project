package com.vlad.repin.gym.service;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class PriceService {
    public Integer getSubscriptionPrice(int month) {
        int price, count, basePrice = 2000;
        log.debug("month = {}", month);

        if (month <= 2) {
            count = 100;
        } else if (month <= 5) {
            count = 95;
        } else if (month <= 11) {
            count = 90;
        } else count = 85;

        price = (basePrice * count / 100) * month;
        log.debug("price = {}", price);
        return price;
    }
}
