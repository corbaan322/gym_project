package com.vlad.repin.gym.service;


import com.vlad.repin.gym.api.instructors.InstructorInfo;
import com.vlad.repin.gym.api.instructors.UpdateInstructorInfoRequest;
import com.vlad.repin.gym.domain.InstructorsEntity;
import com.vlad.repin.gym.domain.InstructorsRepository;
import com.vlad.repin.gym.exeptions.EntryDoesNotExistException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class InstructorService {
    private final InstructorsRepository instructorsRepository;

    public void saveInstructorData(InstructorInfo request) {
        log.debug("Incoming request = {}", request);
        InstructorsEntity.InstructorsEntityBuilder builder = InstructorsEntity.builder();
        builder.firstName(request.getFirstName())
                .lastName(request.getLastName())
                .workExperience(request.getWorkExperience());
        InstructorsEntity entity = builder.build();
        instructorsRepository.save(entity);
        log.debug("Save success");

    }

    private InstructorInfo toInstructorsData(InstructorsEntity instructorsEntity) {
        log.debug("instructorsEntity = {}", instructorsEntity);
        return new InstructorInfo(
                instructorsEntity.getFirstName(),
                instructorsEntity.getLastName(),
                instructorsEntity.getWorkExperience()
        );
    }

    public List<InstructorInfo> getAllInstructorNames() {
        List<InstructorsEntity> instructorsDataList = instructorsRepository.findAll();
        log.debug("instructorsDataList = {}", instructorsDataList);
        return instructorsDataList.stream()
                .map(this::toInstructorsData)
                .toList();
    }

    public String getInstructorIdByName(String firstName, String lastName)
            throws EntryDoesNotExistException {
        log.debug("firstName = {}, lastName = {}", firstName, lastName);
        InstructorsEntity instructorEntity = instructorsRepository.findByFirstNameAndLastName(firstName, lastName);
        if (instructorEntity == null) throw new EntryDoesNotExistException("EntryDoesNotExistException");
        log.debug("instructorEntity = {}", instructorEntity);
        return instructorEntity.getId().toString();
    }

    public List<InstructorInfo> getInstructorDataByName(String firstName)
            throws EntryDoesNotExistException {
        List<InstructorsEntity> instructorsEntityList = instructorsRepository.findByFirstName(firstName);
        if (instructorsEntityList == null) throw new EntryDoesNotExistException("EntryDoesNotExistException");
        log.debug("instructorsEntityList = {}", instructorsEntityList);
        return instructorsEntityList.stream()
                .map(this::toInstructorsData)
                .toList();
    }

    public void updateInstructorInfo(UpdateInstructorInfoRequest request)
            throws EntryDoesNotExistException {
        InstructorsEntity instructorsEntity = instructorsRepository.findById(request.getId());
        if (instructorsEntity == null) {
            throw new EntryDoesNotExistException("EntryDoesNotExistException");
        }
        instructorsEntity.setFirstName(
                request.getFirstName() != null ? request.getFirstName() : instructorsEntity.getFirstName()
        );
        instructorsEntity.setLastName(
                request.getLastName() != null ? request.getLastName() : instructorsEntity.getLastName()
        );
        instructorsEntity.setWorkExperience(
                request.getWorkExperience() != null ? request.getWorkExperience() : instructorsEntity.getWorkExperience()
        );

        instructorsRepository.save(instructorsEntity);
        log.debug("Update success");

    }

    public void deleteInstructorInfo(String firstName, String lastName)
    throws EntryDoesNotExistException{
        log.debug("firstName = {}, lastName = {}", firstName, lastName);
        InstructorsEntity entity = instructorsRepository.findByFirstNameAndLastName(firstName, lastName);
        if (entity == null) throw new EntryDoesNotExistException("EntryDoesNotExistException");
        log.debug("testEntity = {}", entity);
        instructorsRepository.delete(entity);
        log.debug("Delete success");

    }
}
