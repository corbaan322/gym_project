package com.vlad.repin.gym.service;


import com.vlad.repin.gym.api.gym.GymInfo;
import com.vlad.repin.gym.api.gym.UpdateGymInfoRequest;
import com.vlad.repin.gym.domain.GymInfoEntity;
import com.vlad.repin.gym.domain.GymInfoRepository;
import com.vlad.repin.gym.exeptions.EntryDoesNotExistException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


import java.time.LocalTime;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class GymInfoService {
    private final LocalTime TIME_OPEN = LocalTime.of(10, 0);
    private final LocalTime TIME_CLOSE = LocalTime.of(20, 0);

    private final GymInfoRepository gymInfoRepository;

    public void saveGymInfo(GymInfo request) {
        log.debug("Incoming request = {}", request);
        GymInfoEntity.GymInfoEntityBuilder builder = GymInfoEntity.builder();
        builder.gymName(request.getGymName())
                .location(request.getLocation())
                .phoneNumber(request.getPhoneNumber())
                .openingHours(request.getOpeningHours());
        GymInfoEntity entity = builder.build();
        gymInfoRepository.save(entity);
        log.debug("Save success");
    }

    private GymInfo toGymInfo(GymInfoEntity gymInfo) {
        return new GymInfo(
                gymInfo.getGymName(),
                gymInfo.getLocation(),
                gymInfo.getPhoneNumber(),
                gymInfo.getOpeningHours()
        );
    }

    public List<GymInfo> getAllGymsInfo() {
        List<GymInfoEntity> gymInfoList = gymInfoRepository.findAll();
        return gymInfoList.stream()
                .map(this::toGymInfo)
                .toList();
    }

    public List<GymInfo> getListGymInfoByName(String name)
            throws EntryDoesNotExistException {
        List<GymInfoEntity> gymInfoEntityList = gymInfoRepository.findByGymName(name);
        if (gymInfoEntityList == null) throw new EntryDoesNotExistException("EntryDoesNotExistException");
        log.debug("gymInfoEntityList = {}", gymInfoEntityList);
        return gymInfoEntityList.stream()
                .map(this::toGymInfo)
                .toList();
    }

    public void updateGymInfo(UpdateGymInfoRequest request)
            throws EntryDoesNotExistException {
        GymInfoEntity gymInfoEntity = gymInfoRepository.findById(request.getId());
        if (gymInfoEntity == null) {
            throw new EntryDoesNotExistException("EntryDoesNotExistException");
        }
        gymInfoEntity.setGymName(
                request.getGymName() != null ? request.getGymName() : gymInfoEntity.getGymName()
        );
        gymInfoEntity.setLocation(
                request.getLocation() != null ? request.getLocation() : gymInfoEntity.getLocation()
        );
        gymInfoEntity.setPhoneNumber(
                request.getPhoneNumber() != null ? request.getPhoneNumber() : gymInfoEntity.getPhoneNumber()
        );
        gymInfoEntity.setOpeningHours(
                request.getOpeningHours() != null ? request.getOpeningHours() : gymInfoEntity.getOpeningHours()
        );

        gymInfoRepository.save(gymInfoEntity);
        log.debug("Update success");
    }

    public void deleteGymInfo(String gymName, String location) {
        log.debug("Incoming gymName = {}, location = {}", gymName, location);
        GymInfoEntity entity = gymInfoRepository.findByGymNameAndLocation(gymName, location);
        log.debug("Incoming entity = {}", entity);
        gymInfoRepository.delete(entity);
        log.debug("Delete success");
    }

    public boolean chekOpenGymNow() {
        LocalTime currentTime = LocalTime.now();
        return (currentTime.isAfter(TIME_OPEN) && currentTime.isBefore(TIME_CLOSE));
    }
}
