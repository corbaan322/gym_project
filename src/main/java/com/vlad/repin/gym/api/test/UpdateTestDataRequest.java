package com.vlad.repin.gym.api.test;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class UpdateTestDataRequest {
    private int id;
    private String name;
    private LocalDateTime localDateTime;
    private TestType testType;
}
