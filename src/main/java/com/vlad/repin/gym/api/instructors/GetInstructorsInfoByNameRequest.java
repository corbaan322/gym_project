package com.vlad.repin.gym.api.instructors;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetInstructorsInfoByNameRequest {
    private String firstName;
}
