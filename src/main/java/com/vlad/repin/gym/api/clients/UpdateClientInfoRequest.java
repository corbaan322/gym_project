package com.vlad.repin.gym.api.clients;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateClientInfoRequest {
    private int id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String paidMonths;
}
