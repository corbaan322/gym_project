package com.vlad.repin.gym.api.gym;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetGymInfoByNameAndLocationRequest {
    private String gymName;
    private String location;
}
