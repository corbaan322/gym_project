package com.vlad.repin.gym.api.clients;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientInfo {
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String paidMonths;
}
