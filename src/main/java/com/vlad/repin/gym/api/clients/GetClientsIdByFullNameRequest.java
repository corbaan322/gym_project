package com.vlad.repin.gym.api.clients;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetClientsIdByFullNameRequest {
    private String firstName;
    private String lastName;
}
