package com.vlad.repin.gym.api.gym;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateGymInfoRequest {
    private int id;
    private String gymName;
    private String location;
    private String phoneNumber;
    private String openingHours;
}
