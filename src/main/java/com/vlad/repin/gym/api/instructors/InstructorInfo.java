package com.vlad.repin.gym.api.instructors;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class InstructorInfo {
    private String firstName;
    private String lastName;
    private Integer workExperience;
}
