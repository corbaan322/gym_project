package com.vlad.repin.gym.api.gym;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GymInfo {
    private String gymName;
    private String location;
    private String phoneNumber;
    private String openingHours;
}
