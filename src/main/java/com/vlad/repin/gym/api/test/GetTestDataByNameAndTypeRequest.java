package com.vlad.repin.gym.api.test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetTestDataByNameAndTypeRequest {
    private String name;
    private TestType testType;
}
