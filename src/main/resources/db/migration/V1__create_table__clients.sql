create table clients
(
    id           SERIAL       NOT NULL PRIMARY KEY,
    first_name   VARCHAR(150) NOT NULL,
    last_name    VARCHAR(150) NOT NULL,
    phone_number VARCHAR(50)  NOT NULL,
    paid_months  VARCHAR(50)  NOT NULL
);
