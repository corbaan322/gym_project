create table gym_info
(
    id            SERIAL       NOT NULL PRIMARY KEY,
    gym_name      VARCHAR(150) NOT NULL,
    location      VARCHAR(150) NOT NULL,
    phone_number  VARCHAR(150) NOT NULL,
    opening_hours VARCHAR(150) NOT NULL
);
