INSERT INTO gym_info (gym_name, location, phone_number, opening_hours)
VALUES ('GYM FOR REAL MAN', 'Moscow, Kutuzovsky Prospekt, 55', '89146379999', '10:00 - 20:00'),
       ('Gold`s Gym', 'USA, 360 Hampton Dr, Venice, CA 90291', '+13103926004', '05:30-23:00');
ALTER SEQUENCE gym_info_id_seq RESTART WITH 100;
