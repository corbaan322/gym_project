INSERT INTO clients (first_name, last_name, phone_number, paid_months)
VALUES ('Victor', 'Yakovlef', '89146372699', '5'),
       ('Yana', 'Yakovlefa', '89146372684', '5'),
       ('John', 'Python', '89616378884', '12');
ALTER SEQUENCE clients_id_seq RESTART WITH 4;
