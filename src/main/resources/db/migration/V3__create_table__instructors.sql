create table instructors
(
    id              SERIAL       NOT NULL PRIMARY KEY,
    first_name      VARCHAR(150) NOT NULL,
    last_name       VARCHAR(150) NOT NULL,
    work_experience INTEGER      NOT NULL
);
