create table test
(
    id              SERIAL                   NOT NULL PRIMARY KEY,
    name            VARCHAR(150)             NOT NULL,
    local_date_time TIMESTAMP WITH TIME ZONE NOT NULL,
    test_type       VARCHAR(20)              NOT NULL
);
