INSERT INTO instructors (first_name, last_name, work_experience)
VALUES ('Oleg', 'Smirnof', 10),
       ('Maria', 'Grishina', 5),
       ('Sergei', 'Bolotov', 1);
ALTER SEQUENCE instructors_id_seq RESTART WITH 4;